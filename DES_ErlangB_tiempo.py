import simpy
import numpy as np
import sys
import random
import matplotlib.pyplot as plt
from scipy.special import factorial


class Servicio(object):

    """Esta clase representa a un Servicio"""

    #Definicion de Constructor
    def __init__(self, entorno, Lambda, Mu, capacidad):
        self.entorno = entorno
        self.Lambda = Lambda
        self.Mu = Mu
        self.capacidad = capacidad
        self.Resource = simpy.Resource(entorno, capacidad)

    #Definicion de Métodos
    def procesarNacimiento(self, Lambda):
        return random.expovariate(Lambda)
        # return np.random.exponential(1 / Lambda)

    def procesarMuerte(self, Mu):
        return random.expovariate(1 / Mu)
        # return np.random.exponential(Mu)

    def getUsuariosConectados(self):
        return self.Resource.count

    def imprimirEstadisticas(self):
        print('  %d de %d canales están asignados.' % (self.Resource.count, self.Resource.capacity))
        print('  Usuarios conectados:', self.Resource.users)
        print('  Eventos siendo atendidos:', self.Resource.queue)



class Simulacion:

    """Esta clase representa a una Simulación"""
    # Definicion de Variables
    probabilidad_Bloqueo = 0
    contadorBloqueos = 0
    contadorArribos = 0
    umbralArribos = 0
    tiempoObservacion = []
    tiempoObservacion1 = []
    probabilidades_Estado = []
    probabilidades_Estado1 = []
    estados = []
    tiempoEspera = []
    numUsuarios = []
    tiempoObservacion2 = []
    tiempoTotalSimulacion = 0



def simulacionEventosDiscretos(entorno, servicio, simulacion ):
    while True:
        yield entorno.timeout(servicio.procesarNacimiento(servicio.Lambda))
        simulacion.contadorArribos = simulacion.contadorArribos + 1
        simulacion.tiempoObservacion.append([entorno.now, servicio.getUsuariosConectados()])
        simulacion.tiempoObservacion1.append(servicio.getUsuariosConectados())
        simulacion.numUsuarios.append(servicio.getUsuariosConectados())
        #print(entorno.now, 'Arribo #{} '.format(simulacion.contadorArribos))
        if servicio.getUsuariosConectados() < servicio.capacidad:
            entorno.process(peticionServicio(entorno, servicio, simulacion))
        elif servicio.getUsuariosConectados() == servicio.capacidad:
            simulacion.contadorBloqueos = simulacion.contadorBloqueos + 1
            #print(entorno.now, 'Bloqueo #{} '.format(simulacion.contadorBloqueos))


def peticionServicio(entorno, servicio, simulacion):
    with servicio.Resource.request() as peticion:
        tiempoArribo=entorno.now
        yield peticion
        yield entorno.timeout(servicio.procesarMuerte(servicio.Mu))
        simulacion.tiempoObservacion.append([entorno.now, servicio.getUsuariosConectados()])
        simulacion.numUsuarios.append(servicio.getUsuariosConectados())
        tiempoPartida=entorno.now
        simulacion.tiempoEspera.append(tiempoPartida - tiempoArribo)
        #print(entorno.now, 'Petición de Servicio a arribo {} '.format(simulacion.contadorArribos))
        #servicio.imprimirEstadisticas()


def sumTiempoTotal(simulacion):
    sum=0
    for x in range(len(simulacion.tiempoObservacion)):
        if x==0:
            time=0;
        else:
            time=simulacion.tiempoObservacion[x-1][0]
        tiempo=simulacion.tiempoObservacion[x][0]-time
        sum=sum+tiempo
    return sum


def probabilidadesEstado(simulacion, servicio):
    probabilidades=[]
    tiempoTotal=sumTiempoTotal(simulacion)
    for i in range(servicio.capacidad+1):
        for x in range(len(simulacion.tiempoObservacion)):
            if simulacion.tiempoObservacion[x][1]==i:
                for y in range(x, len(simulacion.tiempoObservacion)):
                    if simulacion.tiempoObservacion[y][1] != i:
                        break
                probabilidades.append(simulacion.tiempoObservacion[y][0]-simulacion.tiempoObservacion[x][0])
        simulacion.probabilidades_Estado.append(sum(probabilidades)/tiempoTotal)
        probabilidades=[]


def probabilidadesEstado1(simulacion, servicio):
    simulacion.estados = np.zeros(servicio.capacidad+1)
    simulacion.probabilidades_Estado1 = np.zeros(servicio.capacidad+1)
    for x in range(len(simulacion.tiempoObservacion1)):
        simulacion.estados[simulacion.tiempoObservacion1[x]] = simulacion.estados[simulacion.tiempoObservacion1[x]] + 1
    for y in range(servicio.capacidad+1):
        simulacion.probabilidades_Estado1[y]=simulacion.estados[y]/simulacion.contadorArribos

def Pe(s, a, j):
    A=(a**j)/factorial(j)

    sum=0
    for k in range(0, s+1):
        sum=sum+(a**k)/factorial(k)

    return A/sum


def comparacionProbEstado(simulacion, servicio):
    probabilidadesEstado(simulacion, servicio)
    probabilidadesEstado1(simulacion, servicio)

    plt.figure(1)

    x = np.linspace(0.0, servicio.capacidad, servicio.capacidad+1)
    y = [Pe(servicio.capacidad, servicio.Lambda, edo) for edo in x]
    plt.plot(x, y, 'b', label="ProbEdo Teórica")


    plt.xlabel('Numero Servidores')
    plt.ylabel('$P_e$ (probabilidad de estado)')
    plt.title("Probabilidades de estado M/M/"+str(servicio.capacidad)+" con a="+str(servicio.Lambda))

    y1 = simulacion.probabilidades_Estado
    y2 = simulacion.probabilidades_Estado1
    plt.plot(x, y1, 'r', label="ProbEdo Simulada Método 1")
    plt.plot(x, y2, 'g', label="ProbEdo Simulada Método 2")

    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper right', borderaxespad=0.)
    plt.show()


def graficas(simulacion, servicio):
    plt.figure(2)
    plt.hist(simulacion.tiempoEspera)
    plt.xlabel('Tiempo de espera (m)')
    plt.ylabel('Frecuencia')
    plt.title("Histograma M/M/"+str(servicio.capacidad)+" con a="+str(servicio.Lambda))

    simulacion.tiempoObservacion2 = np.zeros(len(simulacion.tiempoObservacion))

    for c in range(len(simulacion.tiempoObservacion)):
        simulacion.tiempoObservacion2[c] = simulacion.tiempoObservacion[c][0]

    plt.figure(3)
    plt.axis([0, 50, 0, servicio.capacidad+1])
    plt.step(simulacion.tiempoObservacion2, simulacion.numUsuarios, where='post')
    plt.xlabel('Tiempo (m)')
    plt.ylabel('Numero de Usuarios')
    plt.title("Observación de Canal M/M/"+str(servicio.capacidad)+" con a="+str(servicio.Lambda))

    plt.show()




#Inicializacion de la simulación

np.random.seed(0)
entorno = simpy.Environment()
Lambda = float(sys.argv[1])
Mu = 1
capacidad = int(sys.argv[2])
servicio = Servicio(entorno, Lambda, Mu, capacidad)
simulacion = Simulacion()
entorno.process(simulacionEventosDiscretos(entorno, servicio, simulacion))

simulacion.tiempoTotalSimulacion=int(sys.argv[3])
entorno.run(until=simulacion.tiempoTotalSimulacion)
simulacion.probabilidad_Bloqueo = simulacion.contadorBloqueos / simulacion.contadorArribos
#print('Probabilidad de Bloqueo {} '.format(simulacion.probabilidad_Bloqueo))
comparacionProbEstado(simulacion, servicio)
graficas(simulacion, servicio)
print(simulacion.probabilidad_Bloqueo)